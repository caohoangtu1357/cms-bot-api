<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{
    public function login(Request $request)
    {
        try {
            $this->validate($request, [
                'username' => 'required|string',
                'password' => 'required|string',
            ]);

            $credentials = $request->only(['username', 'password']);

            if (!$token = Auth::attempt($credentials)) {
                return response()->json(['msg' => 'Unauthorized'], 401);
            }
            return response()->json(["token" => $token, "token_type" => "Bearer", "expire_in" => Auth::factory()->getTTL() * 60]);

        } catch (ValidationException $exception) {
            return response()->json(["msg" => $exception->errors()], 422);

        }
    }
}
